/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica1;

import javax.swing.JOptionPane;

/**
 *
 * @author Nata
 */
public class ListaLigadaCircular {
    

    int tamaño;
    NodoSimple primero;
    NodoSimple ultimo;

    public ListaLigadaCircular() {  //Constructor
        primero = ultimo = null;
        tamaño = 0;
    }

    public boolean esVacia() {
        return primero == null;
    }

    public NodoSimple getPrimero() {
        return primero;
    }

    public NodoSimple getUltimo() {
        return ultimo;
    }

    public void setUltimo(NodoSimple ultimo) {
        this.ultimo = ultimo;
    }
    
    public void CrearPalabra(String palabra)
    {
        primero = ultimo = null;
        tamaño = 0;
        AgregarAPalabra(palabra);
    }
    public void AgregarAPalabra(String palabra)
    {
        for (int i = 0; i < palabra.length(); i++) {
            insertar(palabra.substring(i, i+1), null);
            tamaño ++;
        }
    }

    public String muestraLista() {
        String lista = "";
        NodoSimple x = primero;
        if (this.esVacia()) {
            return ("");
        } else {
            do {
                lista = lista + x.getDato();
                if (x == ultimo) {
                    return lista;
                }
                if (x.getLiga() != primero) {
                    lista = lista + " -> ";
                }
                x = x.getLiga();
            } while (x != primero);
            return lista;

        }

    }

    public String listaStr() {
        String lista = "";
        NodoSimple x = primero;
        if (this.esVacia()) {
            return ("");
        } else {
            do {
                lista = lista + x.getDato();
                x = x.getLiga();
            } while (x != primero);
            return lista;

        }
    }

    public void enlistar(NodoSimple x) {
        if (this.esVacia()) {
            primero = x;
            ultimo = x;
        } else {
            ultimo.setLiga(x);
            ultimo = x;
        }
        x.setLiga(primero);
    }

    public NodoSimple anterior(NodoSimple nodo) {
        NodoSimple x = this.getPrimero();
        while (x.getLiga() != this.getPrimero()) {
            if (x.getLiga() == nodo) {
                return x;
            }
            x = x.getLiga();
        }

        if (nodo == primero && nodo == ultimo) {
            return primero;
        }
        return null;
    }
//    --------------------------------------------------------------------------

    public void reversaLista() {
        NodoSimple p, q, r, antUlti;
        p = getPrimero();
        antUlti = ultimo;
        ultimo = p;
        q = anterior(p);
        while (p != antUlti ){
            r = q;
            q = p;
            p = p.getLiga();
            q.setLiga(r);
        }
        p.setLiga(q);
        primero.setLiga(p);
        primero = p;

    }

    public void insertar(String d, NodoSimple y) {
        NodoSimple x = new NodoSimple(d);
        enlistar(x);
        //conectar(x, ultimo);

    }

    public void conectar(NodoSimple x, NodoSimple y) {
        if (y != null) {
            x.setLiga(y.getLiga());
            y.setLiga(x);
            NodoSimple p = new NodoSimple("");

            if (y == getUltimo()) {
                setUltimo(x);

            }
            x.setLiga(getPrimero());
            if (getPrimero() == null) {
                setUltimo(x);

            }
        }

    }
    
     public ListaLigadaCircular copia(){
         NodoSimple p = new NodoSimple("");
         ListaLigadaCircular c = new ListaLigadaCircular();
         p=getPrimero();
         do{
             c.insertar(p.getDato(),c.getUltimo());
             p=p.getLiga();
         }while(p!=getPrimero());
        return c; 
     }
     
     public void normaliza(){
        NodoSimple p = new NodoSimple("");
        NodoSimple q= new NodoSimple("");
        p=getPrimero();
        q=anterior(p);
        do{
            q.setLiga(null);
            p=getPrimero();
        }while((p!=getPrimero())&&(p.getDato()==""));
     }
     
     public int getTamaño()
     {
         return tamaño;
     }
     // Este método compara las dos listas Nodo para determinar si contienen la misma palabra
     public boolean esIgual(ListaLigadaCircular otra)
     {
         if (tamaño != otra.getTamaño()) return false;
         NodoSimple p = otra.getPrimero();
         NodoSimple c = getPrimero();
         do{
             if ( !(p.getDato().toString().equals(c.getDato().toString())) ) return false;
             c=c.getLiga();
             p=p.getLiga();
         }while(c!=getPrimero());
         return true;
     }
     // Crea un vector de listas circulares con todas las palabras del tamaño indicado
     public ListaLigadaCircular[] subPalabras(int tam)
     {
        int cant = 1 + getTamaño() - tam;
        if (cant < 1) return null;
        String pal = listaStr();
        ListaLigadaCircular[] nLista = new ListaLigadaCircular[cant];
         for (int i = 0; i < cant; i++) {
             ListaLigadaCircular n = new ListaLigadaCircular();
             n.CrearPalabra(pal.substring(i, i + tam));
             nLista[i] = n;
         }
         return nLista;
     }
     
     public String Ordenada()
     {
         char[] caracteres = new char[tamaño];
         char[] caracteresOrdenado = new char[tamaño];
         for (int i = 0; i < tamaño; i++) {
             caracteres[i] = listaStr().charAt(i);
         }
         char mayor = 0;
         int posMayor = 0;
         for (int i = 0; i < tamaño; i++) {
            mayor = 0;
            for (int j = i; j < tamaño; j++) {
                 if (caracteres[j]>mayor) {
                     mayor = caracteres[j];
                     posMayor = j;
                 }
             }
             caracteres[posMayor] = caracteres[i];
             caracteres[i] = mayor;
         }
         
         String palNueva = "";

         for (int i = 0; i < tamaño; i++) {
             caracteresOrdenado[i] = caracteres[tamaño-i-1];
         }
         for (int i = 0; i < tamaño; i++) {
             palNueva = palNueva + caracteresOrdenado[i];
         }
         
         return palNueva;
     }
     // reemplaza un substring por otro en caso de ser posible, claro está
     // tetorna mensajes para mostrar en el panel de usuario
     public String Replace(String _vieja, String _nueva)
     {
         if (_nueva.equals("")) return EliminarParte(_vieja);
         if (_vieja.length() > tamaño)
             return("Error: La palabra que busca es más grande que la que está almacenada");
         NodoSimple nNodo = BuscarSubString(_vieja.toUpperCase());
         if (nNodo == null)
         {
             return "No está la palabra buscada";
         }else{
             ListaLigadaCircular nLista = new ListaLigadaCircular();
             nLista.CrearPalabra(_nueva);
             NodoSimple nUltimoNodo = nNodo.getLiga();
             for(int i = 0; i< _vieja.length()-1;i++)
                 nUltimoNodo = nUltimoNodo.getLiga();
             nLista.getUltimo().setLiga(nUltimoNodo.getLiga());
             tamaño = tamaño + _nueva.length() - _vieja.length();
             nNodo.setLiga(nLista.getPrimero());
         }
         return("Algo pasó,estoy seguro");
     }
     // quita un substring
     public String EliminarParte(String _vieja)
     {
         if (_vieja.length() > tamaño)
             return("Error: La palabra que busca es más grande que la que está almacenada");
         NodoSimple nNodo = BuscarSubString(_vieja.toUpperCase());
         if (nNodo == null)
         {
             return "No está la palabra buscada";
         }else{
             NodoSimple nUltimoNodo = nNodo.getLiga();
             for(int i = 0; i< _vieja.length()-1;i++)
                 nUltimoNodo = nUltimoNodo.getLiga();
             tamaño = tamaño  - _vieja.length();
             nNodo.setLiga(nUltimoNodo.getLiga());
         }
         return("Algo pasó,estoy seguro");
     }
     // este método retorna el nodo anterior en caso de que se encuentre el substring
     NodoSimple BuscarSubString(String _palabra)
     {
         if(!estaLaPalabra(_palabra)) return null;
        ListaLigadaCircular listaNueva = new ListaLigadaCircular();
        listaNueva.CrearPalabra(_palabra.toUpperCase());
        _palabra = _palabra.toUpperCase();
        
        if (esIgual(listaNueva)) {
            return primero.getLiga();
        }else{
            NodoSimple nodoRecorrer = primero;
            for(int i = 0; i<tamaño ;i++)
            {
                if (laPalabraSigue(nodoRecorrer.getLiga(), _palabra))
                {
                    return nodoRecorrer;
                }else{
                    nodoRecorrer = nodoRecorrer.getLiga();
                }
            }
        }
         return (null);
     }
     
     // este método lo que hace es crear una lista de posibles palabras del mismo tamaño del
     // substring para mirar si está o no
     public boolean estaLaPalabra(String _palabra)
     {
        ListaLigadaCircular[] listaSubPalabras = subPalabras(_palabra.length());
        ListaLigadaCircular miLista = new ListaLigadaCircular();
        miLista.CrearPalabra(_palabra.toUpperCase());
        boolean encontroIgual = false;
                
        for (int i = 0; i < listaSubPalabras.length; i++) 
        {
            if (listaSubPalabras[i].esIgual(miLista))
            {
                encontroIgual = true;
                break;
            }
        }
        return encontroIgual;
     }
     //Esta es una funcion recursiva que mira letra por letra si la letra que sigue es lamisma que 
     //en la palabra que se busca y así compara letra por letra hasta que la haya
     public boolean laPalabraSigue(NodoSimple queNodo, String _palabra)
     {
         if (_palabra.length() == 1)
         {
             if(queNodo.dato.equals(_palabra))
                 return true;
             return false;
         }else{
             return (laPalabraSigue(queNodo.getLiga(), _palabra.substring(1)));
         }
     }
     
}
